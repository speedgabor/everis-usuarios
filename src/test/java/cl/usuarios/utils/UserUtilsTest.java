package cl.usuarios.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UserUtilsTest {

	@Test
	void testValidatePassword() {
		String pattern =  "(?=.*[A-Z])(?=.*[a-z])(?=.*\\d).*";
		assertTrue(UserUtils.validatePassword("lsAz$1", pattern));
	}

}
