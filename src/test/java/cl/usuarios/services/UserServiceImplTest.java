package cl.usuarios.services;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cl.usuarios.models.entity.User;

@SpringBootTest
class UserServiceImplTest {

	@Autowired
	private IUserService userService;
	
	@Test
	void testFindById() {
		User user = userService.findById(1L);
		assertTrue(user.getName().equals("Luis"));
	}

}
