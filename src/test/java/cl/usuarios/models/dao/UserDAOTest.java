package cl.usuarios.models.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import cl.usuarios.models.entity.User;


@SpringBootTest
@AutoConfigureTestDatabase
class UserDAOTest {
	/*@Autowired
    private TestEntityManager entityManager;*/
	
	@Autowired
	private UserDAO userDAO;

	@Test
	void testFindByEmail() {
		// given
	    User usuario = new User();
	    usuario.setName("Luis");
	    usuario.setEmail("luis@gmail.com");
	    usuario.setPassword("1234");
	    userDAO.save(usuario);
	    
	    // when
	    User found = userDAO.findByEmail(usuario.getEmail());
	 
	    // then
	    assertThat(found.getName())
	      .isEqualTo(usuario.getName());
	}

}
