package cl.usuarios.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import cl.usuarios.services.UserServiceException;

/**
 * Clase que controla las excepciones de los rest.
 * @author leach
 *
 */

@ControllerAdvice
@RestController
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

			
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        logger.debug("Controlando exception de validación de datos");
        ApiErrorResponse response = new ApiErrorResponse();
        response.setMessage(ex.getBindingResult().toString());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(UserServiceException.class)
    @ResponseBody
    ResponseEntity<ApiErrorResponse> handleControllerException(HttpServletRequest req, UserServiceException ex) {
        logger.debug("Controlando exception de negocio");
        logger.error("Error de negocio",ex);
        ApiErrorResponse response = new ApiErrorResponse();
        response.setMessage(ex.getMessage());
        return new ResponseEntity<ApiErrorResponse>(response, HttpStatus.BAD_REQUEST);
    }
}
