package cl.usuarios.controllers;

/**
 * Clase que encapsula el mensaje de error para los rest
 * 
 * @author leach
 *
 */
public class ApiErrorResponse {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
