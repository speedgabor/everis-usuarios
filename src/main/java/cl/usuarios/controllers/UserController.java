package cl.usuarios.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.usuarios.models.entity.User;
import cl.usuarios.services.IUserService;
import cl.usuarios.services.UserServiceException;

@RestController
public class UserController {
		
	@Autowired
	private IUserService userService;
	
	@GetMapping("/listar")
	public List<User> listar(){
		return userService.findAll().stream().map(user ->{
			return user;
		}).collect(Collectors.toList());
	}
	
	@GetMapping("/ver/{id}")
	public User detalle(@PathVariable Long id) {
		User user = userService.findById(id);
		
		return user;
	}
	
	
	@PostMapping("/crear")
	@ResponseStatus(HttpStatus.CREATED)
	public User crear(@Valid @RequestBody User usuario) throws UserServiceException {
		return userService.create(usuario);
		
	}
	
	@PutMapping("/editar")
	@ResponseStatus(HttpStatus.CREATED)
	public User editar(@Valid @RequestBody User user) throws UserServiceException {
		
        return userService.modify(user);
	}
	
	@DeleteMapping("/eliminar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminar(@PathVariable Long id) {
		userService.deleteById(id);
	}
}
