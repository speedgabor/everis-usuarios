package cl.usuarios.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * Clase encargada de autorizar los endpoints
 * @author leach
 *
 */

@RefreshScope
@Configuration
@EnableResourceServer
public class EveResourceServer extends ResourceServerConfigurerAdapter {
	@Autowired
	private JwtTokenStore tokenStore;
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenStore(tokenStore);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
        http.headers().frameOptions().disable();
        
		http.authorizeRequests().antMatchers("/api/security/oauth/**").permitAll()
		.antMatchers(HttpMethod.GET, "/listar").permitAll()
		.antMatchers(HttpMethod.POST,"/crear").permitAll()
		.antMatchers("/h2-console/**").permitAll()
		.anyRequest().authenticated();
		
	}	
}
