package cl.usuarios.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cl.usuarios.models.dao.UserDAO;
import cl.usuarios.models.entity.User;

/**
 * Clase que crea el UserDetails, necesario para el proceso de autenticación.
 * @author leach
 *
 */

@Service
public class EveUserDetails implements UserDetailsService {
	static final Logger logger = LoggerFactory.getLogger(EveUserDetails.class);
	
	@Autowired
	private UserDAO userDAO;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User userDb = userDAO.findByEmail(username);
        if (userDb == null) {
        	logger.info("Usuario '" + username + "' no encontrado");
            throw new UsernameNotFoundException("Usuario '" + username + "' no encontrado");
        }
        if (!userDb.getActive()) {
        	logger.info("Usuario '" + username + "' no activo");
        	throw new UsernameNotFoundException("Usuario '" + username + "' no activo");
        }
        List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
        roles.add(new SimpleGrantedAuthority("ADMIN"));
        
        logger.debug("Usuario auntenticado: " + username);

        return new org.springframework.security.core.userdetails.User(userDb.getEmail(), userDb.getPassword(), 
        		userDb.getActive(), true, true, true, roles);
        
	}

}
