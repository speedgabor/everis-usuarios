package cl.usuarios.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import cl.usuarios.models.dao.UserDAO;
import cl.usuarios.models.entity.User;
import cl.usuarios.services.IUserService;

/**
 * Clase utilizada para agregar informaci�n de negocio al jwt y registrarlo en la BD. Tambi�n ac� se realiza la actualizaci�n
 * de la fecha y hora del �ltimo login.
 * @author leach
 *
 */

@Component
public class InfoAdicionalToken implements TokenEnhancer {
	static final Logger logger = LoggerFactory.getLogger(InfoAdicionalToken.class);

	@Autowired
	private UserDAO userDAO;
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Map<String, Object> info = new HashMap<String, Object>();
		
		User usuario = userDAO.findByEmail(authentication.getName());
		logger.info("Usuario BD: " + usuario.getEmail());
		info.put("idNegocio", usuario.getId());
		info.put("nombre", usuario.getName());
		info.put("activo", usuario.getActive());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		
		//Actualizo login exitoso y token
		usuario.setLastLogin(new Date());
		usuario.setModifyAt(new Date());
		usuario.setToken(accessToken.getValue());
		userDAO.save(usuario);
		
		return accessToken;
	}

}
