package cl.usuarios.models.dao;

import org.springframework.data.repository.CrudRepository;

import cl.usuarios.models.entity.User;

public interface UserDAO extends CrudRepository<User, Long> {
	public User findByEmail(String email);
}
