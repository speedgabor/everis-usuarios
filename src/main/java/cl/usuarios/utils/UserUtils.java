package cl.usuarios.utils;

import java.util.regex.Matcher;

/**
 * Clase utilitaria para la validación de la password
 * @author leach
 *
 */

public class UserUtils {
	
	public static boolean validatePassword(String password, String patternStr) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
		//return false;
	}
}
