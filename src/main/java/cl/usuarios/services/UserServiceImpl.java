package cl.usuarios.services;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.usuarios.models.dao.UserDAO;
import cl.usuarios.models.entity.User;
import cl.usuarios.utils.UserUtils;

/**
 * Clase de servicio que maneja toda la lógica de negocio para la administración de un usuario.
 * @author leach
 *
 */

@Service
public class UserServiceImpl implements IUserService {
	static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Value("${everist.usuarios.passwordPattern}")
	private String passwordPattern;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncode;

	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) userDAO.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {
		return userDAO.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public User create(User user) throws UserServiceException{
		logger.info("Patrón de clave: " + passwordPattern);
		User userDb = userDAO.findByEmail(user.getEmail());
		if (userDb != null) {
			throw new UserServiceException("El correo ya registrado");
		}
		if (!UserUtils.validatePassword(user.getPassword(), passwordPattern)) {
			throw new UserServiceException("La clave no respeta el formato");
		}
		user.setPassword(passwordEncode.encode(user.getPassword()));
		user.setActive(true);
		user.setCreateAt(new Date());
		user.setModifyAt(new Date());
		user.setLastLogin(new Date());
		return userDAO.save(user);
	}
	
	@Override
	@Transactional
	public User modify(User user) throws UserServiceException {
		if (user.getId() == null) {
			throw new UserServiceException("Debe indicar el id del usuario");
		}
		User userDb = userDAO.findById(user.getId()).orElse(null);
		if (userDb == null){
			throw new UserServiceException("Usuario no existe para ese id");
		}
		if (!UserUtils.validatePassword(user.getPassword(), passwordPattern)) {
			throw new UserServiceException("La clave no respeta el formato");
		}
		userDb.setName(user.getName());
		userDb.setPassword(passwordEncode.encode(user.getPassword()));
        userDb.setActive(user.getActive());
        userDb.setPhones(user.getPhones());
        userDb.setModifyAt(new Date());
        return userDAO.save(userDb);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		userDAO.deleteById(id);
	}
}
