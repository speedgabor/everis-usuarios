package cl.usuarios.services;

/**
 * Clase Exception para controlar los errores de negocio del servicio UserService.
 * @author leach
 *
 */
public class UserServiceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -252405620162455377L;

	public UserServiceException(String message) {
        super(message);
    }
}
