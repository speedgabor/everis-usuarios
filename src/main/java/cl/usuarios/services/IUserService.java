package cl.usuarios.services;

import java.util.List;

import cl.usuarios.models.entity.User;

public interface IUserService {
	public List<User> findAll();
	public User findById(Long id);
	
	public User create(User user) throws UserServiceException;
	
	public User modify(User user) throws UserServiceException;
	
	public void deleteById(Long id);
}
